struct A *x; //指针可以提前声明
struct A
{
    int x;
    struct B
    {
        int x;
    }y;
};
int f()
{
    struct B y;//struct 里面的struct 定义外面也能用；
    y.x=0;
    int f=0;//考察函数和变量命名空间不同
    f++;
    return f;
}
int g(int x,int ,int ,char *); //考察函数提前声明和里面的可以匿名(如果数据不能声明函数的话忽略这个吧。。。)

int g(int y,int x,int z,char *a)
{
    return x+y+z;
}
int a[10];
int main()
{
    int b=(int)a;
    int c=(int *)a; //数组类型强制转换
    printf("b=%d c=%d\n",b,c);
    printf("%d\n",f(1,2,3)); //考察无参数函数可以传进去一些参数来调用他
	return 0;
}
